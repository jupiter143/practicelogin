
// Swift
//
// Add this to the header of your file, e.g. in ViewController.swift

import FBSDKLoginKit
import AuthenticationServices
import FirebaseAuth

class ViewController: UIViewController, LoginButtonDelegate {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnLoginFB: UIButton!
    @IBOutlet weak var signinApple: UIButton!
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    let loginButton = FBLoginButton()
    let appleSignin = AppleSignInClient()
    let buttonApple = ASAuthorizationAppleIDButton()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let token = AccessToken.current, !token.isExpired {
            
                   let token = token.tokenString
                   let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, picture"], tokenString: token, version: nil, httpMethod: .get)
                   request.start(completionHandler: {connection, result, error in
                       print("\(result!)")
                   })

               } else {
                   loginButton.center = view.center
                   loginButton.delegate = self
                   loginButton.permissions = ["public_profile","email"]
                               }
        
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        let token = result?.token?.tokenString
               let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                                parameters: ["fields": "id, email, name, picture.type(large)"],
                                                                tokenString: token,
                                                                version: nil,
                                                                httpMethod: .get)
       
               request.start(completionHandler: {connection, result, error in
                   
                   let object = result as? [String: Any]
                   self.lblName.text = (object!["name"] as! String)
                   self.lblEmail.text = (object!["email"] as! String)
                   let picture = object!["picture"] as? [String: Any]
                   let data = picture!["data"] as? [String: Any]
                   let url = data!["url"]!
                   let myUrl = URL(string: url as! String)
                   let dataR = try? Data(contentsOf: myUrl!)
                let image = UIImage(data: dataR!)
                self.userProfileImageView.image = image
               })
        self.btnLoginFB.setTitle("Logout", for: .normal)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        self.btnLoginFB.setTitle("Login with Facebook", for: .normal)
        self.lblName.text = ""
        self.lblEmail.text = ""
        self.userProfileImageView.image = UIImage()
    }
    
    @IBAction func btnLoginFB(_ sender: Any) {
        loginButton.sendActions(for: .touchUpInside)
    }
    
    @objc func signInWithApple(sender: ASAuthorizationAppleIDButton) {
            appleSignin.handleAppleIdRequest(block: { fullName, email, token in
                // receive data in login class.
            })

    }

    @IBAction func btnLoginApple(_ sender: Any) {
        buttonApple.addTarget(self, action: #selector(signInWithApple(sender: )), for: .touchUpInside)
        buttonApple.sendActions(for: .touchUpInside)
    }
    
    @IBAction func btnLoginPhone(_ sender: Any) {
        
        
    }
    
}
