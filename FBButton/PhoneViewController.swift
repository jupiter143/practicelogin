//
//  PhoneViewController.swift
//  FBButton
//
//  Created by BTB_003 on 11/27/20.
//

import UIKit
import FirebaseAuth
import FirebaseCore

class PhoneViewController: UIViewController {

    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldCode: UITextField!
    var verification_id: String? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        textFieldCode.isHidden = true
      
    }
    
    @IBAction func btnSubmitCode(_ sender: Any) {
        if !textFieldCode.isHidden {
            if !textFieldPhone.text!.isEmpty{
                Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                   PhoneAuthProvider.provider().verifyPhoneNumber(textFieldPhone.text!, uiDelegate: nil, completion: {
                       verificationID, error in
                      if(error != nil){
                           return
                       }
                       else{
       
                          self.verification_id = verificationID
                          self.textFieldCode.isHidden = false
                       }
                     })
            }
            else{
                print("Please Enter Your Mobile Number")
            }
        }else{
            if verification_id != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: textFieldCode.text!)
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if (error != nil){
                        print(error.debugDescription)
                    }
                    else{
                        print("Authentication Success With - " + (authData?.user.phoneNumber! ?? "No Phone Number"))
                    }
                })
            }
            else{
                print("Error In Getting Verification ID")
            }
         }
    }
    
    
}
